---
layout: markdown_page
title: University Classes
---

To view the slides of the classes nicely, use [Deckset](http://www.decksetapp.com/).
In the future we will add pdfs of the slides to the repo.

Regarding tests - If you make the test more than a week later than the class,
notify someone (Job) to check your answers.

## Recordings & Slides

- [Internal Recordings](https://drive.google.com/drive/u/0/folders/0B41DBToSSIG_NlNFLUEwQ2JHSVk) link accessible to GitLab employees only.
- [The GLU Repository](https://gitlab.com/gitlab-org/University) contains the slides and
documents of the classes.

## Classes

### Nov 19, 2015

- [GitLab 8.2](https://gitlab.com/gitlab-org/University/blob/master/classes/8.2.md)
- [Make the test](http://goo.gl/forms/9PnmhiNzEa)

### Nov 12, 2015

- [Upcoming in EE](https://gitlab.com/gitlab-org/University/blob/master/classes/upcoming_in_ee.md)
- [Big files in Git (Git LFS, Annex)](https://gitlab.com/gitlab-org/University/blob/master/classes/git_lfs_and_annex.md)
- [Make the test](http://goo.gl/forms/RFsNK9fKuj)
